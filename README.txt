The HEAD branch is not used because between two stable Drupal releases, the
strings are changing very often. Therefore, only stable releases are translated.
Translation happens in the corresponding branches.

== Currently active Maintainers are:

Sanduhrs Group Manager/Maintainer
Joachim Namyslo Maintainer

=== User Guide 

As Maintainer you took the respomnsibility to translate Drupals UI as well as Drupals User Guide.

- Project:
  https://www.drupal.org/project/user_guide
-Issues:
  https://www.drupal.org/project/issues/user_guide?status=Open&component=DE+translation
- Stageing-Environment:
   https://www.staging.devdrupal.org/de/docs/user_guide/de/index.html
   u/p drupal 

==Roles

=== Project Manager

    Your Job is to talk to Module developers and create issues 
    if you discover strings that would be parsed out by Drupals malformed HTML filter when they get translated. 
    In an Ideal world, you are also willing and able to patch modules you discover such issues in.

=== Maintainer

    Your Job is to make sure that: 
    
    - Drupals UI, 
    - Online-Help and 
    - localized Documentation 
    
    is as understandable as possible to newbees and beginners as well as to evaluators, as possible. 
    We do not focus on Developers in this Project at all. 
    You are also reviewing the work of others and give feedback to them by useing the projects issue queue

=== Translation Community Moderator

    You are responsible for suggesting strings and correct suggestions of translators, 
    as well as giving feedback to them , after doing UI review, to support Project Maintainers.

=== Translator

    You're job is to suggesting translations

== Why we are not gendering here?

Gendering in German is much more complicated than e.g. in Englisch. 
The only way to make sure Drupals UI is gender proove is to spin a new 
fork of this project, where engaged people will make sure to rewrite every string effected, by hand. 
At the time of Updateing that file there are still 873461 strings to translate. 
This is rising with any update or new release of a theme or a module. 

As long as we have not achived a rest of exactly 0 strings to translate in this project, 
it doesn't make any sense to start the support of propper gendering. 
So we'll propperly never get there becuase there is no massive and 
coordinated effort to finish this project right now.